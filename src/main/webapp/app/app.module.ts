import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { CicdSharedModule } from 'app/shared/shared.module';
import { CicdCoreModule } from 'app/core/core.module';
import { CicdAppRoutingModule } from './app-routing.module';
import { CicdHomeModule } from './home/home.module';
import { CicdEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    CicdSharedModule,
    CicdCoreModule,
    CicdHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    CicdEntityModule,
    CicdAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class CicdAppModule {}
