/**
 * View Models used by Spring MVC REST controllers.
 */
package com.multiplica.cicd.web.rest.vm;
