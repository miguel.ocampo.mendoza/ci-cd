package com.multiplica.cicd;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.multiplica.cicd");

        noClasses()
            .that()
                .resideInAnyPackage("com.multiplica.cicd.service..")
            .or()
                .resideInAnyPackage("com.multiplica.cicd.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..com.multiplica.cicd.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
