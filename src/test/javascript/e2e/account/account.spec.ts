import { browser, element, by, ExpectedConditions as ec } from 'protractor';

import { NavBarPage, SignInPage, PasswordPage, SettingsPage } from '../page-objects/jhi-page-objects';

const expect = chai.expect;

describe('account', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  const username = process.env.E2E_USERNAME || 'admin';
  const password = process.env.E2E_PASSWORD || 'admin';
  let passwordPage: PasswordPage;
  let settingsPage: SettingsPage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage(true);
  });

  it('debe fallar al iniciar sesión con una contraseña incorrecta', async () => {
    const expect1 = 'Bienvenid@, CI-CD!';
    const value1 = await element(by.css('h1')).getText();
    expect(value1).to.eq(expect1);
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, 'foo');

    const expect2 = 'Failed to sign in! Please check your credentials and try again.';
    const value2 = await element(by.css('.alert-danger')).getText();
    expect(value2).to.eq(expect2);
  });

  it('debe iniciar sesión correctamente con la cuenta de administrador', async () => {
    await browser.get('/');
    signInPage = await navBarPage.getSignInPage();

    const expect1 = 'Login';
    const value1 = await element(by.className('username-label')).getText();
    expect(value1).to.eq(expect1);
    await signInPage.autoSignInUsing(username, password);

    const expect2 = 'You are logged in as user "admin".';
    await browser.wait(ec.visibilityOf(element(by.id('home-logged-message'))));
    const value2 = await element(by.id('home-logged-message')).getText();
    expect(value2).to.eq(expect2);
  });

  it('debe poder actualizar la configuración', async () => {
    settingsPage = await navBarPage.getSettingsPage();

    const expect1 = `User settings for [${username}]`;
    const value1 = await settingsPage.getTitle();
    expect(value1).to.eq(expect1);
    await settingsPage.save();

    const expect2 = 'Settings saved!';
    const alert = element(by.css('.alert-success'));
    const value2 = await alert.getText();
    expect(value2).to.eq(expect2);
  });

  it('debe fallar al actualizar la contraseña cuando se usa una contraseña incorrecta', async () => {
    passwordPage = await navBarPage.getPasswordPage();

    expect(await passwordPage.getTitle()).to.eq(`Password for [${username}]`);

    await passwordPage.setCurrentPassword('wrong_current_password');
    await passwordPage.setPassword('new_password');
    await passwordPage.setConfirmPassword('new_password');
    await passwordPage.save();

    const expect2 = 'An error has occurred! The password could not be changed.';
    const alert = element(by.css('.alert-danger'));
    const value2 = await alert.getText();
    expect(value2).to.eq(expect2);
    settingsPage = await navBarPage.getSettingsPage();
  });

  it('debe poder actualizar la contraseña', async () => {
    passwordPage = await navBarPage.getPasswordPage();

    expect(await passwordPage.getTitle()).to.eq(`Password for [${username}]`);

    await passwordPage.setCurrentPassword(password);
    await passwordPage.setPassword('newpassword');
    await passwordPage.setConfirmPassword('newpassword');
    await passwordPage.save();

    const successMsg = 'Password changed!';
    const alert = element(by.css('.alert-success'));
    const alertValue = await alert.getText();
    expect(alertValue).to.eq(successMsg);
    await navBarPage.autoSignOut();
    await navBarPage.goToSignInPage();
    await signInPage.autoSignInUsing(username, 'newpassword');

    // change back to default
    await navBarPage.goToPasswordMenu();
    await passwordPage.setCurrentPassword('newpassword');
    await passwordPage.setPassword(password);
    await passwordPage.setConfirmPassword(password);
    await passwordPage.save();

    // wait for success message
    const alertValue2 = await alert.getText();
    expect(alertValue2).to.eq(successMsg);
  });

  it('debe navegar a la página de error 404 no encontrado en una ruta no existente y mostrar la barra de navegación del usuario si existe una autenticación válida', async () => {
    // don't sign out and refresh page with non existing route
    await browser.get('/this-is-link-to-non-existing-page');

    // should navigate to 404 not found page
    const url = await browser.getCurrentUrl();
    expect(url).to.endWith('404');

    // as user is admin then should show admin menu to user
    await navBarPage.clickOnAdminMenu();
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
